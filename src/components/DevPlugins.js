// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
import {addDevPlugin, removeDevPlugin} from "../reducers/developer";
const React = qu4rtet.require("react");
const {RightPanel} = qu4rtet.require("./components/layouts/Panels");
const {Component} = React;
const {withRouter} = qu4rtet.require("react-router-dom");
const {FormattedMessage} = qu4rtet.require("react-intl");
const {Card} = qu4rtet.require("@blueprintjs/core");
const {connect} = qu4rtet.require("react-redux");
const fs = qu4rtet.require("fs");

let refresh_allowed_in = null; // used to debounce the fs watcher.
let enabled = {};

const addToWatched = (path, callback) => {
  if (!enabled[path]) {
    fs.watch(
      path,
      {
        recursive: true,
        persistent: true
      },
      (evtType, fileName) => {
        updatePlugins(path);
      }
    );
    enabled[path] = true;
  } else {
    console.log("already watching", path);
  }
};

const updatePlugins = path => {
  let timestamp = new Date().getTime();
  if (!refresh_allowed_in || refresh_allowed_in < timestamp) {
    setTimeout(() => {
      store.dispatch(addDevPlugin(path));
    }, 1500);
  } else if (refresh_allowed_in) {
    console.log("Not updating yet, still", refresh_allowed_in - timestamp);
  }
  refresh_allowed_in = timestamp + 2000; // 2s off refresh.
};

class PluginInput extends Component {
  constructor(props) {
    super(props);
    this.state = {path: this.props.path || null};
  }
  setPath = e => {
    this.setState({path: e.target.value});
  };
  componentDidCatch(error, info) {
    this.setState({
      hasError: true,
      error
    });
  }  
  render() {
    return (
      <div className="pt-form-group">
        <label className="pt-label">Local Development Plugin</label>
        <div className="pt-form-content">
          <input
            id="example-form-group-input-a"
            className="pt-input"
            placeholder="Plugin Path"
            style={{minWidth: "60%"}}
            type="text"
            dir="auto"
            onBlur={e => {
              this.setState({path: e.target.value});
            }}
          />
          <div className="pt-form-helper-text">
            Enter a path to a plugin package
          </div>
          <button
            onClick={e => {
              addToWatched(this.state.path, this.props.addDevPlugin.bind(this));
              this.props.addDevPlugin(this.state.path);
            }}
            className="pt-button pt-icon-play pt-intent-primary"
          />
          <button className="pt-button pt-icon-eject" />
          <button className="pt-button pt-icon-trash pt-intent-danger" />
        </div>
      </div>
    );
  }
}

class _DevPlugins extends Component {
  constructor(props) {
    super(props);
    this.state = {blankInputs: 0};
  }
  render() {
    const {devPlugins} = this.props;
    return (
      <RightPanel
        key="develTools"
        title={<FormattedMessage id="plugins.developer.develTools" />}>
        <Card>
          <h5>Development plugins</h5>
          <button
            className="pt-button pt-intent-primary"
            onClick={e => {
              this.setState({blankInputs: this.state.blankInputs + 1});
            }}>
            Add a dev plugin
          </button>
          {[...Array(this.state.blankInputs)].map((_, i) => (
            <PluginInput {...this.props} />
          ))}
          {devPlugins.map(path => {
            <PluginInput {...this.props} path={path} />;
          })}
        </Card>
      </RightPanel>
    );
  }
}

export const DevPlugins = connect(
  (state, ownProps) => {
    return {
      devPlugins: state.developer.devPlugins || []
    };
  },
  {addDevPlugin, removeDevPlugin}
)(withRouter(_DevPlugins));
