"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DevPlugins = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _developer = require("../reducers/developer");

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } } // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


const React = qu4rtet.require("react");
const { RightPanel } = qu4rtet.require("./components/layouts/Panels");
const { Component } = React;
const { withRouter } = qu4rtet.require("react-router-dom");
const { FormattedMessage } = qu4rtet.require("react-intl");
const { Card } = qu4rtet.require("@blueprintjs/core");
const { connect } = qu4rtet.require("react-redux");
const fs = qu4rtet.require("fs");

let refresh_allowed_in = null; // used to debounce the fs watcher.
let enabled = {};

const addToWatched = (path, callback) => {
  if (!enabled[path]) {
    fs.watch(path, {
      recursive: true,
      persistent: true
    }, (evtType, fileName) => {
      updatePlugins(path);
    });
    enabled[path] = true;
  } else {
    console.log("already watching", path);
  }
};

const updatePlugins = path => {
  let timestamp = new Date().getTime();
  if (!refresh_allowed_in || refresh_allowed_in < timestamp) {
    setTimeout(() => {
      store.dispatch((0, _developer.addDevPlugin)(path));
    }, 1500);
  } else if (refresh_allowed_in) {
    console.log("Not updating yet, still", refresh_allowed_in - timestamp);
  }
  refresh_allowed_in = timestamp + 2000; // 2s off refresh.
};

class PluginInput extends Component {
  constructor(props) {
    super(props);

    this.setPath = e => {
      this.setState({ path: e.target.value });
    };

    this.state = { path: this.props.path || null };
  }

  componentDidCatch(error, info) {
    this.setState({
      hasError: true,
      error
    });
  }
  render() {
    return React.createElement(
      "div",
      { className: "pt-form-group" },
      React.createElement(
        "label",
        { className: "pt-label" },
        "Local Development Plugin"
      ),
      React.createElement(
        "div",
        { className: "pt-form-content" },
        React.createElement("input", {
          id: "example-form-group-input-a",
          className: "pt-input",
          placeholder: "Plugin Path",
          style: { minWidth: "60%" },
          type: "text",
          dir: "auto",
          onBlur: e => {
            this.setState({ path: e.target.value });
          }
        }),
        React.createElement(
          "div",
          { className: "pt-form-helper-text" },
          "Enter a path to a plugin package"
        ),
        React.createElement("button", {
          onClick: e => {
            addToWatched(this.state.path, this.props.addDevPlugin.bind(this));
            this.props.addDevPlugin(this.state.path);
          },
          className: "pt-button pt-icon-play pt-intent-primary"
        }),
        React.createElement("button", { className: "pt-button pt-icon-eject" }),
        React.createElement("button", { className: "pt-button pt-icon-trash pt-intent-danger" })
      )
    );
  }
}

class _DevPlugins extends Component {
  constructor(props) {
    super(props);
    this.state = { blankInputs: 0 };
  }
  render() {
    const { devPlugins } = this.props;
    return React.createElement(
      RightPanel,
      {
        key: "develTools",
        title: React.createElement(FormattedMessage, { id: "plugins.developer.develTools" }) },
      React.createElement(
        Card,
        null,
        React.createElement(
          "h5",
          null,
          "Development plugins"
        ),
        React.createElement(
          "button",
          {
            className: "pt-button pt-intent-primary",
            onClick: e => {
              this.setState({ blankInputs: this.state.blankInputs + 1 });
            } },
          "Add a dev plugin"
        ),
        [].concat(_toConsumableArray(Array(this.state.blankInputs))).map((_, i) => React.createElement(PluginInput, this.props)),
        devPlugins.map(path => {
          React.createElement(PluginInput, _extends({}, this.props, { path: path }));
        })
      )
    );
  }
}

const DevPlugins = exports.DevPlugins = connect((state, ownProps) => {
  return {
    devPlugins: state.developer.devPlugins || []
  };
}, { addDevPlugin: _developer.addDevPlugin, removeDevPlugin: _developer.removeDevPlugin })(withRouter(_DevPlugins));